## Como usar

Você deve ter em seu computador o git, composer e XAMP. Caso não tenha siga o tutorial que está na primeira atividade do EAD.

Após tudo funcionando. Abra o termina e faça os comandos abaixo:

```bash
git clone https://gitlab.com/ifms-aulas/frameworks.git
```
Entre na pasta do projeto

```bash
cd frameworks
```

Depois de escolher a branch que você quer usar, faça o checkout para ela. Normalmente vou nomear como Aula01, Aula02, etc. E assim o comando deve ser: git checkout Aula01

```bash 
git checkout nome-da-branch
```

Depois de fazer o checkout para a branch que você quer usar, faça o pull para atualizar a branch (caso você já tenha ela no seu PC)

```bash
git pull
```

Abra o projeto no seu VS code ou outra IDE de programação, se for no VS COde faça:

```bash
code .
```

!!! ATENÇÃO !!! Crie o arquivo .env baseado no .env.example. Apenas control c + control v e renomeie para .env
Ajuste o arquivo .env com as configurações do seu banco de dados

Instale as dependências do projeto

```bash
composer update
```

Gere a chave do projeto

```bash
php artisan key:generate
```

Rode as migrations

```bash
php artisan migrate
```

Rodar as seeds (para cada seeder mude o arquivo após o =)

```bash
php artisan db:seed --class=AlunosTableSeeder 
```

Rode o servidor

```bash
php artisan serve
```

[//]: # (glpat-Rj2x9B-o4AbaCQHrXbzq)
